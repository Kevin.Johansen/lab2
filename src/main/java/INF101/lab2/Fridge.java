package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {

	List<FridgeItem> fridgeContent = new ArrayList<FridgeItem>();
	int totalSpace = 20;
	
	@Override
	public int nItemsInFridge() {
		return fridgeContent.size();
	}

	@Override
	public int totalSize() {
		return totalSpace;
	}

	@Override
	public boolean placeIn(FridgeItem item) {
		if (fridgeContent.size() < 20) {
			fridgeContent.add(item);
			return true;
		} else {
			return false;
		}
		
	}

	@Override
	public void takeOut(FridgeItem item) {
		if (fridgeContent.contains(item)) {
			fridgeContent.remove(item);
		} else {
			throw new NoSuchElementException("No such item exists in the fridge.");
		}
		
	}

	@Override
	public void emptyFridge() {
		fridgeContent.clear();
	}

	@Override
	public List<FridgeItem> removeExpiredFood() {
		List<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
		for (int i = 0; i < fridgeContent.size(); i++) {
			if (fridgeContent.get(i).hasExpired()) {
				expiredFood.add(fridgeContent.get(i));
			}
		}
		for (int i = 0; i < expiredFood.size(); i++) {
			takeOut(fridgeContent.get(i));
		}
		return expiredFood;
	}

}
